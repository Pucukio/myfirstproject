
fun main() {
    print("Masukkan sebuah bilangan: ")
    val number = readln().toInt()

    val isPrime = isPrimeNumber(number)

    if (isPrime) {
        println("$number adalah bilangan prima.")
    } else {
        println("$number bukan bilangan prima.")
    }
}

fun isPrimeNumber(number: Int): Boolean {
    if (number <= 1) {
        return false
    }

    for (i in 2..number / 2) {
        if (number % i == 0) {
            return false
        }
    }

    return true
}
